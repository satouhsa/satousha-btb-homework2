import React, {Component} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ToolBar from './ToolBar';

class User extends Component {


 
 

  constructor(props) {
    super(props);
    this.state = {
      Seemore1:"",
      Seemore2:"",
      Seemore3:"",
      Seemore4:"",
      Seemore5:"",

      
      data: [
        {
          title: 'Billie Eilish',
          profile:
            'https://media.resources.festicket.com/www/artists/BillieEilish.jpg',
          photo:
            'https://cdn.theatlantic.com/thumbor/xaSHYVZFp7hFvL-xFRWs0TdGOUA=/0x110:3594x1982/960x500/media/img/mt/2020/01/GettyImages_1202225047/original.jpg',
          description:
            'The Grammy Award for Best New Artist has been awarded since 1959. Years reflect the year in which the Grammy Awards were handed out, for records released in the previous year. The award was not presented in 1967.',
        },
        {
          title: 'Romeo Beckham',
          profile: 'https://gossipgist.com/uploads/21216/romeo.png',
          photo:
            'https://www.hellomagazine.com/imagenes/travel/2020072994437/romeo-beckham-girlfriend-mia-enjoy-staycation-cornwall/0-452-721/romeo-beckham-mia-italy-t.jpg',
          description:
            " Brooklyn Joseph Beckham was born at The Portland Hospital in Marylebone, London, the son of David Beckham and Victoria Adams (later Beckham). It is often reported that he was named Brooklyn because he was conceived in Brooklyn, New York; according to Victoria Beckham's 2014",
        },
        {
          title: 'David Beckham',
          profile:
            'https://m.media-amazon.com/images/M/MV5BMjA3ODA0MDI4OF5BMl5BanBnXkFtZTgwMjEyNzI3MDE@._V1_UY317_CR131,0,214,317_AL_.jpg',
          photo:
            'https://static.standard.co.uk/s3fs-public/thumbnails/image/2020/01/02/08/beckhams0201a.jpg?width=1000&height=614&fit=bounds&format=pjpg&auto=webp&quality=70&crop=16:9,offset-y0.5',
          description:
            'David Robert Joseph Beckham OBE[4] (UK: /ˈbɛkəm/;[5] born 2 May 1975) is an English former professional footballer, the current president & co-owner of Inter Miami CF and co-owner of Salford City.[6] He played for Manchester United, Preston North End, Real Madrid, Milan, LA Galaxy, Paris Saint-Germain and the England national team, for which he held the appearance record for an outfield player until 2016. He is the first English player to win league titles in four countries: England, Spain, the United States and France.',
        },
        {
          title: 'Taylor Swift',
          profile:
            'https://pmcvariety.files.wordpress.com/2020/01/taylor-swift-variety-cover-5-16x9-1000.jpg?w=1000',
          photo: 'https://i.insider.com/5f6f3dd274fe5b0018a8da4c?width=700',
          description:
            ' Taylor Alison Swift is an American singer-songwriter. Her narrative songwriting, which often centers around her personal life, has received widespread critical plaudits and media coverage. Born in West Reading, Pennsylvania, Swift relocated to Nashville, Tennessee in 2004 to pursue a career in country music.',
        },
        {
          title: 'Angelina Jolie',
          profile:
            'https://www.devdiscourse.com/remote.axd?https://devdiscourse.blob.core.windows.net/devnews/15_04_2020_15_48_11_2767629.png?width=1280',
          photo:
            'https://www.kissottawa.com/wp-content/uploads/sites/59/2020/03/image0-e1584732226641-1024x576.jpeg',
          description:
            "Selena Marie Gomez is an American singer, actress, and producer. Born and raised in Texas, Gomez began her career by appearing on the children's television series Barney & Friends.",
        },
      ],
    };
  }


  Seemore1 = () =>{
    alert(this.state.data[0].description)
  }
  Seemore2 = () =>{
    alert(this.state.data[1].description)
  }
  Seemore3 = () =>{
    alert(this.state.data[2].description)
  }
  Seemore4 = () =>{
    alert(this.state.data[3].description)
  }
  Seemore5 = () =>{
    alert(this.state.data[4].description)
  }


  render() {
   
    return (
      <SafeAreaView>
        <ScrollView>
          <View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={styles.header}>
                <Image
                  style={{height: 48, width: 48, borderRadius: 40}}
                  source={{
                    uri: this.state.data[0].profile,
                  }}
                />
              </View>

              <View>
                <Text style={styles.loginText}>{this.state.data[0].title}</Text>
              </View>

              <View>
                    <TouchableOpacity
                     
                      onPress={this.Seemore1.bind(this)}
                      underlayColor="#fff">
                      <Icon style={{marginLeft:218,marginTop:15}} name="ellipsis-h" size={30} color="#000" />
                    </TouchableOpacity>
               
              </View>
            </View>

            <View>
              <Text
              
                style={{paddingLeft: 20, paddingRight: 20, marginBottom: 13, fontFamily:"BebasNeue-Regular"}}
                numberOfLines={2}
                ellipsizeMode="tail">
                {this.state.data[0].description} 
              </Text>
            </View>

            <View>
              <Image
                style={{height: 200, width: 380, marginLeft: 19}}
                source={{
                  uri: this.state.data[0].photo,
                }}
              />
            </View>
          </View>
          {/* titile */}
          <View>
            <ToolBar/>
          </View>
          {/* button */}
          


          {/* the second */}
          <View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={styles.header}>
                <Image
                  style={{height: 48, width: 48, borderRadius: 40}}
                  source={{
                    uri: this.state.data[1].profile,
                  }}
                />
              </View>

              <View>
                <View>
                  <Text style={styles.loginText}>
                    {this.state.data[1].title}
                  </Text>
                </View>
              </View>
              <View>
                   <TouchableOpacity
                     
                     onPress={this.Seemore2.bind(this)}
                     underlayColor="#fff">
                     <Icon style={{marginLeft:170,marginTop:15}} name="ellipsis-h" size={30} color="#000" />
                   </TouchableOpacity>
              </View>
            </View>

            <View>
              <Text
                style={{paddingLeft: 20, paddingRight: 20, marginBottom: 13,fontFamily:"FontAwesome"}}
                numberOfLines={2}
                ellipsizeMode="tail">
                {this.state.data[1].description}
              </Text>
            </View>
            <View>
              <Image
                style={{height: 200, width: 380, marginLeft: 19}}
                source={{
                  uri: this.state.data[1].photo,
                }}
              />
            </View>
          </View>
          {/* end */}
          {/* title */}
          <View>
            <ToolBar/>
          </View>
          {/* button */}
         

          {/* the third */}
          <View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={styles.header}>
                <Image
                  style={{height: 48, width: 48, borderRadius: 40}}
                  source={{
                    uri: this.state.data[2].profile,
                  }}
                />
              </View>

              <View>
                <View>
                  <Text style={styles.loginText}>
                    {this.state.data[2].title}
                  </Text>
                </View>
              </View>
              <View>
                  <TouchableOpacity
                     
                     onPress={this.Seemore3.bind(this)}
                     underlayColor="#fff">
                     <Icon style={{marginLeft:170,marginTop:15}} name="ellipsis-h" size={30} color="#000" />
                   </TouchableOpacity>
              </View>
            </View>
            <View>
              <Text
                style={{paddingLeft: 20, paddingRight: 20, marginBottom: 13,fontFamily:"FontAwesome"}}
                numberOfLines={2}
                ellipsizeMode="tail">
                {this.state.data[2].description}
              </Text>
            </View>
            <View>
              <Image
                style={{height: 200, width: 380, marginLeft: 19}}
                source={{
                  uri: this.state.data[2].photo,
                }}
              />
            </View>
          </View>
          {/* titile */}
          <View>
            <ToolBar/>
          </View>
          {/* button */}
         

          {/* end of third */}
          {/* four */}
          <View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={styles.header}>
                <Image
                  style={{height: 48, width: 48, borderRadius: 40}}
                  source={{
                    uri: this.state.data[3].profile,
                  }}
                />
              </View>

              <View>
                <View>
                  <Text style={styles.loginText}>
                    {this.state.data[3].title}
                  </Text>
                </View>
              </View>
              <View>
                   <TouchableOpacity
                     
                     onPress={this.Seemore4.bind(this)}
                     underlayColor="#fff">
                     <Icon style={{marginLeft:185,marginTop:15}} name="ellipsis-h" size={30} color="#000" />
                   </TouchableOpacity>
              </View>
            </View>
            <View>
              <Text
                style={{paddingLeft: 20, paddingRight: 20, marginBottom: 13,fontFamily:"FontAwesome"}}
                numberOfLines={2}
                ellipsizeMode="tail">
                {this.state.data[3].description}
              </Text>
            </View>
            <View>
              <Image
                style={{height: 200, width: 380, marginLeft: 19}}
                source={{
                  uri: this.state.data[3].photo,
                }}
              />
            </View>
          </View>
          {/* title */}
          <View>
            <ToolBar/>
          </View>
          {/* btton */}
         

          {/* end four */}
          {/* five */}

          <View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={styles.header}>
                <Image
                  style={{height: 48, width: 48, borderRadius: 40}}
                  source={{
                    uri: this.state.data[4].profile,
                  }}
                />
              </View>

              <View>
                <View>
                  <Text style={styles.loginText}>
                    {this.state.data[4].title}
                  </Text>
                </View>
              </View>
              <View>
                   <TouchableOpacity
                     
                     onPress={this.Seemore5.bind(this)}
                     underlayColor="#fff">
                     <Icon style={{marginLeft:170,marginTop:15}} name="ellipsis-h" size={30} color="#000" />
                   </TouchableOpacity>
              </View>
            </View>
            <View>
              <Text
                style={{paddingLeft: 20, paddingRight: 20, marginBottom: 13,fontFamily:"FontAwesome"}}
                numberOfLines={2}
                ellipsizeMode="tail">
                {this.state.data[4].description}
              </Text>
            </View>
            <View>
              <Image
                style={{height: 200, width: 380, marginLeft: 19}}
                source={{
                  uri: this.state.data[4].photo,
                }}
              />
            </View>
          </View>
          {/* titile */}
          <View>
            <ToolBar/>
          </View>
          {/* button */}
          

          {/* end of five */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 100,
  },
  storiesList: {
    flex: 1,
    flexDirection: 'row',
    marginRight: 15,
    marginLeft: 15,
  },
  storiesItem: {
    width: 130,
    height: 185,
    backgroundColor: 'powderblue',
    margin: 5,
    borderRadius: 10,
  },
  imgStories: {
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    borderRadius: 10,
  },
  safe_area_view: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    // backgroundColor: 'red',
    padding: 15,
  },
  logofb: {
    width: 200,
    height: 40,
  },
  profileImgContainer: {
    marginLeft: 38,
    height: 50,
    width: 50,
    borderRadius: 40,
  },
  profileImg: {
    height: 48,
    width: 48,
    borderRadius: 40,
  },
  subprofile: {
    height: 50,
    width: 50,
    borderRadius: 40,
    margin: 10,
  },
  padd_subprofile: {
    marginRight: 28,
    marginLeft: 28,
  },
  postbtn: {
    backgroundColor: '#dc143c',
  },
  loginScreenButton: {
    marginRight: 10,
    // marginLeft:40,
    // marginTop:10,
    paddingTop: 10,
    paddingBottom: 5,
    backgroundColor: '#f5f5f5',
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#fff',
    width: '92%',
  },
  loginText: {
    color: '#000',
    textAlign: 'center',
    paddingLeft: 2,
    paddingRight: 2,
    padding: 25,
  },
  placeholderstyle: {
    padding: 6,
    flex: 1,
    backgroundColor: '#ccc',
    flexDirection: 'row',
    marginTop: 8,
  },
});
export default User;
