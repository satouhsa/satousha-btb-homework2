import React, { Component } from 'react'
import { View ,StyleSheet, TextInput, Text,Image,TouchableOpacity,Animated} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

 class ToolBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
          like: 0,
          share: 0,
          comment: 0,
          onClicked: false,
          changeFont: true,
         
        };
        this.handlerButtonOnClick = this.handlerButtonOnClick.bind(this);
      }
    
      
      handlerButtonOnClick() {
        this.setState({
          onClicked: true,
        });
      }
    
      btnLike = () => {
        this.setState({
          like: this.state.like + 1,
        });
      };
   
      btnShare = () => {
          alert('you shared the post');
        this.setState({
            
          share: this.state.share + 1,
        });
      };
      btnComment = () => {
          this.setState({
              comment:this.state.comment + 1,
          })
      }

    render() {

        const {like} = this.state;
        const {share} = this.state;
        const {comment} = this.state;
        return (
        

            <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={styles.container}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginLeft: 8,
                  marginTop: 5,
                }}>
                <View style={{width: 50, flex: 2, height: 50}}>
                  <View style={styles.countContainer}>
                    <Text style={styles.titleBtn}>{like}&nbsp;Like</Text>
                  </View>
                  <TouchableOpacity style={styles.button} onPress={this.btnLike}>
                    <Text style={styles.btnLike}>
                      <Icon name="thumbs-up" size={20} color="#000" />
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{width: 50, flex: 2, height: 50}}>
                  <View style={styles.countContainer}>
                    <Text style={styles.titleBtn}>{comment}&nbsp;Comment</Text>
                  </View>
                  <TouchableOpacity style={styles.button} onPress={this.btnComment}>
                    <Text style={styles.btnLike}>
                      <Icon name="comment" size={20} color="#000" />
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{width: 50, flex: 2, height: 50}}>
                  <View style={styles.countContainer}>
                    <Text style={styles.titleBtn}>{share}&nbsp;Share</Text>
                  </View>
                  <TouchableOpacity style={styles.button} onPress={this.btnShare}>
                    <Text style={styles.btnLike}>
                      <Icon name="share" size={20} color="#000" />
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.placeholderstyle}></View>
            </View>
          </View>
                   
                    
          
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      paddingHorizontal: 10,
    },
    button: {
      alignItems: 'center',
      width: '100%',
    },
    placeholderstyle: {
      padding: 1,
      flex: 1,
      backgroundColor: '#eee',
      flexDirection: 'row',
      marginTop: 8,
    },
    titleBtn: {
      marginBottom: 0,
    },
    btnLike: {
      color: '#000',
      bottom: 5,
    },
    countContainer: {
      alignItems: 'center',
      padding: 10,
    },
  });
  
export default ToolBar;

