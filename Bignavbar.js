import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  SafeAreaView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import User from './User';

class Bignavbar extends Component {
  onPressBtn = () => {
    alert('Ok');
  };
  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <ScrollView>
          <View style={styles.header}>
            <Image
              source={{
                uri:
                  'https://download.logo.wine/logo/Facebook/Facebook-Logo.wine.png',
              }}
              style={styles.logofb}
            />
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{width: 100, height: 50}}>
              <TouchableHighlight
                style={[
                  styles.profileImgContainer,
                  {borderColor: 'green', borderWidth: 1},
                ]}>
                <Image
                  source={{
                    uri:
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRL50oqYpBZX17oH3MxWI6BpdzguDDN0iybCw&usqp=CAU',
                  }}
                  style={styles.profileImg}
                />
              </TouchableHighlight>
            </View>
            <View
              style={{
                width: 50,
                flex: 2,
                height: 50,
              }}>
              <TouchableOpacity
                style={styles.loginScreenButton}
                onPress={this.onPressBtn.bind(this)}
                underlayColor="#fff">
                <Text style={styles.loginText}>What's on your mind ?</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.placeholderstyle}></View>
          <View style={styles.padd_subprofile}>
            <ScrollView horizontal={true}>
              <Image
                source={{
                  uri:
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRL50oqYpBZX17oH3MxWI6BpdzguDDN0iybCw&usqp=CAU',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://cdn.i-scmp.com/sites/default/files/styles/768x768/public/d8/images/methode/2019/09/13/983d2672-d53f-11e9-a556-d14d94601503_image_hires_180604.jpg?itok=VXIvNYkb&v=1568369168',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://i.insider.com/5e820b04671de06758588fb8?width=1100&format=jpeg&auto=webp',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://www.halffullnotempty.com/wp-content/uploads/media.media_.c4955b62-8ee3-4ee9-9cd3-f5da1b8c5c87.original1024.jpg',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://public.potaufeu.asahi.com/8ade-p/picture/22121024/94e7d5c10ff8ba2975c0c71e5cb29aeb.jpg',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri: 'https://i.ytimg.com/vi/pmanD_s7G3U/maxresdefault.jpg',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://image.freepik.com/free-vector/anime-girl-character_146237-78.jpg',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://besthqwallpapers.com/Uploads/17-2-2018/41098/thumb2-zero-two-manga-anime-characters-pink-hair-darling-in-the-franxx.jpg',
                }}
                style={styles.subprofile}
              />
            </ScrollView>
          </View>
          {/* stroy */}
          <View style={styles.placeholderstyle}></View>
          <View style={styles.storiesList}>
            <View style={styles.storiesItem}>
              <Image
                source={{
                  uri:
                    'https://www.cloverdalereporter.com/wp-content/uploads/2020/01/20298787_web1_CAWS122-124_2020_044508.jpg',
                }}
                style={styles.imgStories}
              />
              {/* the create story */}
              <View style={{marginTop: -55}}>
                <View
                  style={{
                    position: 'absolute',
                    backgroundColor: 'white',
                    width: 200,
                    height: 200,
                  }}>
                  <Text style={{marginLeft: 32, color: 'blue', marginTop: 10}}>
                    {' '}
                    Create
                  </Text>
                  <Text style={{marginLeft: 15}}>Create a Story</Text>
                </View>
              </View>
            </View>
            <View style={styles.container}>
              <ScrollView horizontal={true}>
                <View style={styles.storiesItem}>
                  <Image
                    source={{
                      uri:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSkpp8ekwlfG4ot76mpdBD91Dv5LoYQGQowWg&usqp=CAU',
                    }}
                    style={styles.imgStories}
                  />
                  {/* put  */}
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://static.bangkokpost.com/media/content/20200214/c1_3526964.jpg',
                      }}
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        paddingLeft: 10,
                        paddingRight: 10,
                        padding: 150,
                      }}>
                      Billie Eilish
                    </Text>
                  </View>
                </View>
                <View style={styles.storiesItem}>
                  <Image
                    source={{
                      uri:
                        'https://img1.looper.com/img/gallery/the-best-anime-movies-you-can-watch-on-netflix-right-now/intro-1584125852.jpg',
                    }}
                    style={styles.imgStories}
                  />
                  {/* put */}
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQfqRZ81sIpUq41HEoMNIJuS8B264umpamUFw&usqp=CAU',
                      }}
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        paddingLeft: 10,
                        paddingRight: 10,
                        padding: 150,
                      }}>
                      Taylor Swift
                    </Text>
                  </View>
                </View>
                <View style={styles.storiesItem}>
                  <Image
                    source={{
                      uri:
                        'https://i.pinimg.com/originals/76/fa/eb/76faeb9c818efdf76cf066aea3685a80.jpg',
                    }}
                    style={styles.imgStories}
                  />
                  {/* put */}
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://media1.popsugar-assets.com/files/thumbor/hLNfvE46E2WgfgOh00bRCbHcvCM/562x135:2177x1750/fit-in/2048xorig/filters:format_auto-!!-:strip_icc-!!-/2019/09/09/020/n/1922398/3794d5c55d76e058296c92.97678413_/i/Selena-Gomez.jpg',
                      }}
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        paddingLeft: 10,
                        paddingRight: 10,
                        padding: 150,
                      }}>
                      Selena Gomez
                    </Text>
                  </View>
                </View>
                <View style={styles.storiesItem}>
                  <Image
                    source={{
                      uri:
                        'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Jennie_at_the_Sprite_Waterbomb_Festival_2018_%281%29.jpg/170px-Jennie_at_the_Sprite_Waterbomb_Festival_2018_%281%29.jpg',
                    }}
                    style={styles.imgStories}
                  />
                  {/* put */}
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://akns-images.eonline.com/eol_images/Entire_Site/2020524/rs_1200x1200-200624123242-1200-gigi-hadid.cm.62420.jpg?fit=around|1080:1080&output-quality=90&crop=1080:1080;center,top',
                      }}
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        paddingLeft: 10,
                        paddingRight: 10,
                        padding: 150,
                      }}>
                      Gigi Hadid
                    </Text>
                  </View>
                </View>
                <View style={styles.storiesItem}>
                  <Image
                    source={{
                      uri:
                        'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-0/p526x296/121127202_363052165038761_5122724015536999625_n.jpg?_nc_cat=106&_nc_sid=8bfeb9&_nc_ohc=QmvlAJHIr5IAX-luv2M&_nc_ht=scontent.fpnh7-1.fna&tp=6&oh=9940cbf2ab1e3f5daa9b0b8b6079acba&oe=5FB19030',
                    }}
                    style={styles.imgStories}
                  />
                  {/* put */}
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://imageproxy.themaven.net//https%3A%2F%2Fwww.biography.com%2F.image%2FMTQyMDA0OTY4OTA2MzAyNTI2%2Fkendall-jenner_gettyimages-477504950jpg.jpg',
                      }}
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        paddingLeft: 10,
                        paddingRight: 10,
                        padding: 150,
                      }}>
                      Kendall Jenner
                    </Text>
                  </View>
                </View>
                <View style={styles.storiesItem}>
                  <Image
                    source={{
                      uri:
                        'https://cnet1.cbsistatic.com/img/glzfXa3w2IvUUeT22ifnHCQxaoM=/940x0/2019/06/11/02e36daa-1122-4056-81d0-0a1fd040fdf6/avenger-03.jpg',
                    }}
                    style={styles.imgStories}
                  />
                  {/* put */}
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://ichef.bbci.co.uk/news/1024/cpsprodpb/17159/production/_107435549_bellaafp.jpg',
                      }}
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        paddingLeft: 10,
                        paddingRight: 10,
                        padding: 150,
                      }}>
                      Bella Hadid
                    </Text>
                  </View>
                </View>
              </ScrollView>
            </View>
          </View>
          <View style={styles.placeholderstyle}></View>
          <View>
            <ScrollView>
              {/* <ListPost/> */}
              <User />
            </ScrollView>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 100,
  },
  storiesList: {
    flex: 1,
    flexDirection: 'row',
    marginRight: 15,
    marginLeft: 15,
  },
  storiesItem: {
    width: 130,
    height: 185,
    backgroundColor: 'powderblue',
    margin: 5,
    borderRadius: 10,
  },
  imgStories: {
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    borderRadius: 10,
  },
  safe_area_view: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    // backgroundColor: 'red',
    padding: 15,
  },
  logofb: {
    width: 200,
    height: 40,
  },
  profileImgContainer: {
    marginLeft: 38,
    height: 50,
    width: 50,
    borderRadius: 40,
  },
  profileImg: {
    height: 48,
    width: 48,
    borderRadius: 40,
  },
  subprofile: {
    height: 50,
    width: 50,
    borderRadius: 40,
    margin: 10,
  },
  padd_subprofile: {
    marginRight: 28,
    marginLeft: 28,
  },
  postbtn: {
    backgroundColor: '#dc143c',
  },
  loginScreenButton: {
    marginRight: 10,
    // marginLeft:40,
    // marginTop:10,
    paddingTop: 10,
    paddingBottom: 5,
    backgroundColor: '#f5f5f5',
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#fff',
    width: '92%',
  },
  loginText: {
    color: 'blue',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    padding: 8,
    fontSize: 16,
  },
  placeholderstyle: {
    padding: 6,
    flex: 1,
    backgroundColor: '#ccc',
    flexDirection: 'row',
    marginTop: 8,
  },
});

export default Bignavbar;
